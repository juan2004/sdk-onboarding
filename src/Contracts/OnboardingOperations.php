<?php

namespace Placeto\OnboardingSdk\Contracts;

use Placeto\OnboardingSdk\Helpers\Settings;
use Placeto\OnboardingSdk\Requests\ValidateRequest;
use Placeto\OnboardingSdk\Responses\Response;

abstract class OnboardingOperations
{
    protected Settings $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    abstract public function identityValidationService(ValidateRequest $identityValidateRequest): Response;

    abstract public function consultationService(int $requestId): Response;
}
