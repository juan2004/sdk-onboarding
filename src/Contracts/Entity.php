<?php

namespace Placeto\OnboardingSdk\Contracts;

use Placeto\OnboardingSdk\Helpers\ArrayHelper;
use Placeto\OnboardingSdk\Traits\LoaderTrait;

abstract class Entity
{
    use LoaderTrait;

    abstract public function toArray(): array;

    protected function arrayFilter(array $array): array
    {
        return ArrayHelper::filter($array);
    }
}
