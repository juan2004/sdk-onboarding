<?php

namespace Placeto\OnboardingSdk\Responses;

use Placeto\OnboardingSdk\Contracts\Entity;

class Response extends Entity
{
    protected ?array $response = [];

    public function __construct($data)
    {
        $this->response = $data;
    }

    public function getResponse(): ?array
    {
        return $this->response;
    }

    public function getRequestId(): int
    {
        return $this->getResponse()['data']['request_id'];
    }

    public function getUrlResponse(): string
    {
        return $this->getResponse()['data']['url'];
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'response' => $this->response,
        ]);
    }
}
