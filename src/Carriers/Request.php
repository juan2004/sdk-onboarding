<?php

namespace Placeto\OnboardingSdk\Carriers;

use GuzzleHttp\Exception\BadResponseException;
use Placeto\FreshsalesSdk\Exceptions\OnboardingServiceException;
use Placeto\OnboardingSdk\Contracts\OnboardingOperations;
use Placeto\OnboardingSdk\Helpers\ArrayHelper;
use Placeto\OnboardingSdk\Requests\ValidateRequest;
use Placeto\OnboardingSdk\Responses\Response;
use Throwable;

class Request extends OnboardingOperations
{
    private const ENDPOINT_IDENTITY_VALIDATION_SERVICE = 'request';
    private const ENDPOINT_CONSULTATION_SERVICE = 'query';

    private function makeRequest(string $url, string $action, array $arguments = [])
    {
        try {
            $data = ArrayHelper::filter($arguments);

            $response = $this->settings->getClient()->$action($url, [
                'json' => $data,
                'headers' => $this->settings->getHeaders(),
            ]);

            $result = $response->getBody()->getContents();
        } catch (BadResponseException $e) {
            $result = $e->getResponse()->getBody()->getContents();
        } catch (Throwable $e) {
            throw OnboardingServiceException::fromServiceException($e);
        }

        return json_decode($result, true);
    }

    public function identityValidationService(ValidateRequest $identityValidateRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_IDENTITY_VALIDATION_SERVICE), 'post', $identityValidateRequest->toArray());

        return new Response($result);
    }

    public function consultationService(int $requestId): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_IDENTITY_VALIDATION_SERVICE . '/' . $requestId), 'get');

        return new Response($result);
    }
}
