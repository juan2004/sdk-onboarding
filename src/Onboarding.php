<?php

namespace Placeto\OnboardingSdk;

use Placeto\OnboardingSdk\Exceptions\OnboardingException;
use Placeto\OnboardingSdk\Helpers\Settings;
use Placeto\OnboardingSdk\Requests\ValidateRequest;
use Placeto\OnboardingSdk\Responses\Response;

class Onboarding
{
    protected Settings $settings;

    public function __construct(array $data)
    {
        $this->settings = new Settings($data);
    }

    public function identityValidationService($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $identityValidationServiceRequest = new ValidateRequest($dataRequest);
        }

        if (!($identityValidationServiceRequest instanceof ValidateRequest)) {
            throw OnboardingException::forDataNotProvided('Wrong class request');
        }

        return $this->settings->getOnboardingOperations()->identityValidationService($identityValidationServiceRequest);
    }

    public function consultationService($requestId): Response
    {
        return $this->settings->getOnboardingOperations()->consultationService($requestId);
    }
}
