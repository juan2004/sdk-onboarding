<?php

namespace Placeto\OnboardingSdk\Requests;

use Placeto\OnboardingSdk\Contracts\Entity;
use Placeto\OnboardingSdk\Exceptions\OnboardingException;

class ValidateRequest extends Entity
{
    private const MESSAGE_EXCEPTION = 'The field person is mandatory and must be of type array and contain: document_type,
    document, document_issue_date, email, mobile and mobile_country';

    protected array $person;
    protected ?string $locale = null;
    protected ?string $approval_url = null;
    protected ?string $denial_url = null;

    public function __construct(array $data)
    {
        $this->load($data, ['person', 'locale', 'approval_url', 'denial_url']);
    }

    public function getPerson(): array
    {
        if (isset($this->person['document_type']) && isset($this->person['document']) && isset($this->person['document_issue_date'])
            && isset($this->person['email']) && isset($this->person['mobile']['mobile']) && isset($this->person['mobile']['mobile_country'])) {
            return $this->person;
        } else {
            throw OnboardingException::forDataNotProvided(self::MESSAGE_EXCEPTION);
        }
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function getApprovalUrl(): ?string
    {
        return $this->approval_url;
    }

    public function getDenialUrl(): ?string
    {
        return $this->denial_url;
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'person' => $this->getPerson(),
            'locale' => $this->getLocale(),
            'approval_url' => $this->getApprovalUrl(),
            'denial_url' => $this->getDenialUrl(),
        ]);
    }
}
