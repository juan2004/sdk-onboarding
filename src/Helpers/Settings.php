<?php

namespace Placeto\OnboardingSdk\Helpers;

use GuzzleHttp\Client;
use Placeto\OnboardingSdk\Carriers\Request;
use Placeto\OnboardingSdk\Contracts\Entity;
use Placeto\OnboardingSdk\Contracts\OnboardingOperations;
use Placeto\OnboardingSdk\Exceptions\OnboardingException;

class Settings extends Entity
{
    private const MESSAGE1 = 'No api-key provided';
    private const MESSAGE2 = 'No service URL provided to use';

    protected string $apiKey;
    public string $baseUrl = '';
    protected array $headers = [];

    protected int $timeout = 15;
    protected bool $verifySsl = true;

    protected ?Client $client = null;
    protected ?OnboardingOperations $onboardingOperations = null;

    public function __construct(array $data)
    {
        if (!isset($data['apiKey'])) {
            throw OnboardingException::forDataNotProvided(self::MESSAGE1);
        }

        if (!isset($data['baseUrl']) || !filter_var($data['baseUrl'], FILTER_VALIDATE_URL)) {
            throw OnboardingException::forDataNotProvided(self::MESSAGE2);
        }

        if (substr($data['baseUrl'], -1) != '/') {
            $data['baseUrl'] .= '/';
        }

        $allowedKeys = [
            'apiKey',
            'baseUrl',
            'headers',
        ];

        $this->load($data, $allowedKeys);
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function baseUrl(string $endpoint): string
    {
        return $this->baseUrl . $endpoint;
    }

    public function getHeaders(): array
    {
        $this->headers = [
            'Api-key' => $this->getApiKey(),
        ];
        return $this->headers;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function isVerifySsl(): bool
    {
        return $this->verifySsl;
    }

    public function getClient(): Client
    {
        if (!$this->client) {
            $this->client = new Client([
                'timeout' => $this->getTimeout(),
                'connect_timeout' => $this->getTimeout(),
                'verify' => $this->isVerifySsl(),
            ]);
        }

        return $this->client;
    }

    public function getOnboardingOperations(): OnboardingOperations
    {
        if ($this->onboardingOperations instanceof OnboardingOperations) {
            return $this->onboardingOperations;
        } else {
            $this->onboardingOperations = new Request($this);
        }

        return $this->onboardingOperations;
    }

    public function toArray(): array
    {
        return [];
    }
}
