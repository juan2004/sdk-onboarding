<?php

namespace Placeto\OnboardingSdk\Helpers;

class ArrayHelper
{
    /**
     * Filters an array looking for null values and remove it corresponding key.
     */
    public static function filter(array $array): array
    {
        return array_filter($array, function ($item) {
            return !empty($item) || $item === false || $item == '0';
        });
    }
}
