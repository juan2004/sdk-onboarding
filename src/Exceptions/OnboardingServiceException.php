<?php

namespace Placeto\FreshsalesSdk\Exceptions;

use Placeto\OnboardingSdk\Exceptions\OnboardingException;
use Throwable;

class OnboardingServiceException extends OnboardingException
{
    private const MESSAGE = 'Error handling operation';

    public static function fromServiceException(Throwable $e): self
    {
        return new self(self::MESSAGE, 100, $e);
    }
}
