<?php

namespace Placeto\OnboardingSdk\Exceptions;

use Exception;

class OnboardingException extends Exception
{
    public static function forDataNotProvided(string $message = ''): self
    {
        return new self($message);
    }
}
